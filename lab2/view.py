from python_console_menu import *
from colr import *
from model import *
from entities import *

db = DataBase()
db.connect()


class CRUDMenu(AbstractMenu):
    def __init__(self, entityName_):
        self.entityName = entityName_
        super().__init__("Choose an option:")

    def AddEntityInfo(self, entityName_):
        entity = eval(entityName_)()
        for attr, value in entity.__dict__.items():
            if attr == "Id":
                print()
            elif (attr == "VisaRequired" or attr == "IsPaid"):
                setattr(entity, attr, bool(input(attr + ": ")))
            elif (attr == "ResortId" or attr == "Stars" or attr == "ClientId" or attr == "TourId" or attr == "CountryId"
                  or attr == "HotelId" or attr == "Duration" or attr == "Price"):
                setattr(entity, attr, int(input(attr + ": ")))
            # elif (attr == "DepartureDate" or attr == "PurchaseDate"):
            #     setattr(entity, attr, datetime(input(attr + ": ")))
            else:
                setattr(entity, attr, input(attr + ": "))
        if entityName_ == "Country":
            return db.AddCountry(entity)
        if entityName_ == "Resort":
            return db.AddResort(entity)
        if entityName_ == "Hotel":
            return db.AddHotel(entity)
        if entityName_ == "Tour":
            return db.AddTour(entity)
        if entityName_ == "Order":
            return db.AddOrder(entity)
        if entityName_ == "Client":
            return db.AddClient(entity)
        print("Problem inserting entity: given entity name was not found.")
        return -1

    def UDEntityInfo(self, action, entityName_, entityId):
        entity = db.GetEntityById(entityName_, entityId)
        if action == "update":
            if entity == False:
                return False
            for attr, value in entity.__dict__.items():
                if attr == "Id":
                    setattr(entity, attr, entityId)
                else:
                    setattr(entity, attr, input(attr + ": "))
            for attr, value in entity.__dict__.items():
                print(attr, ": ", value)
            if entityName_ == "Country":
                return db.UpdateCountry(entity)
            if entityName_ == "Resort":
                return db.UpdateResort(entity)
            if entityName_ == "Hotel":
                return db.UpdateHotel(entity)
            if entityName_ == "Tour":
                return db.UpdateTour(entity)
            if entityName_ == "Order":
                return db.UpdateOrder(entity)
            if entityName_ == "Client":
                return db.UpdateClient(entity)
            print("Problem updating entity: given entity name was not found.")
            return False
        elif action == "delete":
            db.DeleteEntity(entity)

    def GenerateEntityInfo(self, entityName_, amount):
        if entityName_ == "Tour":
            return db.GenerateTour(amount)
        if entityName_ == "Order":
            return db.GenerateOrder(amount)
        if entityName_ == "Client":
            return db.GenerateClient(amount)
        print("There are no need in generating this entity. Please choose another entity.")

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Get entity", lambda: db.GetEntityById(self.entityName, input("Id:"))))
        self.add_menu_item(MenuItem(102, "Add entity", lambda: self.AddEntityInfo(self.entityName)))
        self.add_menu_item(
            MenuItem(103, "Update entity", lambda: self.UDEntityInfo("update", self.entityName, input("Id:"))))
        self.add_menu_item(
            MenuItem(104, "Delete entity", lambda: self.UDEntityInfo("delete", self.entityName, input("Id:"))))
        self.add_menu_item(MenuItem(105, "Generate entity", lambda: self.GenerateEntityInfo(self.entityName, input(
            "Amount of entities to generate:"))))


class EntityMenu(AbstractMenu):
    def __init__(self):
        super().__init__("Choose the entity to work with:")

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Country", menu=CRUDMenu("Country")))
        self.add_menu_item(MenuItem(102, "Resort", menu=CRUDMenu("Resort")))
        self.add_menu_item(MenuItem(103, "Hotel", menu=CRUDMenu("Hotel")))
        self.add_menu_item(MenuItem(104, "Tour", menu=CRUDMenu("Tour")))
        self.add_menu_item(MenuItem(105, "Order", menu=CRUDMenu("Order")))
        self.add_menu_item(MenuItem(106, "Client", menu=CRUDMenu("Client")))


class MainMenu(AbstractMenu):
    def __init__(self):
        super().__init__(color("---MAIN MENU---", fore='00f8fc'))

    def getBool(self, input_):
        if (input_.lower() == "true" or int(input_) == 1):
            return True
        else:
            return False

    def RouteFindFunc(self, action):
        if action == "CountryHotels":
            return db.FindCountryHotels(input("Country IsoCode: "), int(input("Hotel stars: ")))
        if action == "ClientPaid":
            return db.FindClientPaid(int(input("Min price: ")), int(input("Max price: ")),
                                     self.getBool(input("Is order paid: ")))
        if action == "TourHotel":
            return db.FindTourHotel(int(input("Min price: ")), int(input("Max price: ")),
                                    int(input("Duration of stay: ")), int(input("Hotel stars: ")))

    def initialise(self):
        self.add_menu_item(MenuItem(100, color("Exit program", fore='917a81')).set_as_exit_option())
        self.add_menu_item(MenuItem(101, color("CRUD+generate entity menu", fore='ebc634'), menu=EntityMenu()))
        self.add_menu_item(MenuItem(102, color("Get hotels by stars from country", fore='03cafc'),
                                    lambda: self.RouteFindFunc("CountryHotels")))
        self.add_menu_item(MenuItem(103, color("Get clients that paid/didn't pay for order", fore='03cafc'),
                                    lambda: self.RouteFindFunc("ClientPaid")))
        self.add_menu_item(MenuItem(104, color("Find tours by price, duration, hotel stars", fore='03cafc'),
                                    lambda: self.RouteFindFunc("TourHotel")))
