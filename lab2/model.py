import psycopg2
from datetime import time, timedelta, datetime
from entities import *


class DataBase:
    def __init__(self, ):
        self.con = None
        self.cur = None

    def connect(self):
        try:
            self.con = psycopg2.connect(
                database="TourAgency",
                user="postgres",
                password="DipperS13",
                port="5432"
            )
            self.cur = self.con.cursor()
        except(Exception, psycopg2.Error):
            print("Problem connecting to the server. Please try again\n")

    def close(self):
        if self.con:
            self.cur.close()
            self.con.close()
            print("Connection to the database is closed.")

    def Exceptions(self, entityName, operationName):
        print(f"Problem trying to {operationName} {entityName}. Given {entityName} was not {operationName}ed")

    def GetEntityById(self, entityName, id):
        try:
            self.cur.execute("SELECT * FROM \"{0}\" WHERE \"Id\" = {1}".format(entityName, int(id)))
            the_entity = self.cur.fetchone()
        except(Exception, psycopg2.Error):
            self.Exceptions("country", "add")
            return False
        if (the_entity == None or len(the_entity) == 0):
            print("Entity with given id was not found.")
            return False
        print(the_entity)
        entity = eval(entityName)()
        i = 0
        for attr, value in entity.__dict__.items():
            setattr(entity, attr, the_entity[i])
            i += 1
            print(attr, ": ", value)
        return entity

    def AddCountry(self, country):
        try:
            self.cur.execute(
                "INSERT INTO public.\"Country\" (\"Name\", \"IsoCode\", \"CurrencyIsoCode\", \"VisaRequired\") "
                "VALUES(%s, %s, %s, %s) RETURNING \"Id\"",
                (country.Name, country.IsoCode, country.CurrencyIsoCode, bool(country.VisaRequired)))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("country", "add")
            return False
        return id

    def AddResort(self, resort):
        self.cur.execute("SELECT * FROM \"Country\" WHERE \"Id\" = {:d}".format((resort.CountryId)))
        check = self.cur.fetchall()
        if check == None:
            print("Resort with given id was not added. Please, firstly add the corresponding country.")
            return False
        try:
            self.cur.execute("INSERT INTO public.\"Resort\" (\"CountryId\", \"Name\") "
                             "VALUES(%s, %s) RETURNING \"Id\"", (resort.CountryId, resort.Name))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("resort", "add")
            return
        return id

    def AddHotel(self, hotel):
        self.cur.execute("SELECT * FROM \"Resort\" WHERE \"Id\" = {:d}".format((hotel.ResortId)))
        check = self.cur.fetchall()
        if check == None:
            print("Hotel with given id was not added. Please, firstly add the corresponding resort.")
            return False
        try:
            self.cur.execute("INSERT INTO public.\"Hotel\" (\"ResortId\", \"Name\", \"Stars\") "
                             "VALUES(%s, %s, %s) RETURNING \"Id\"", (hotel.ResortId, hotel.Name, hotel.Stars))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("hotel", "add")
            return
        return id

    def AddTour(self, tour):
        self.cur.execute("SELECT * FROM \"Hotel\" WHERE \"Id\" = {:d}".format((tour.HotelId)))
        check = self.cur.fetchall()
        if check == None:
            print("Tour with given id was not added. Please, firstly add the corresponding hotel.")
            return False
        try:
            self.cur.execute("INSERT INTO public.\"Tour\" (\"HotelId\", \"DepartureDate\", \"DepartureCity\", "
                             "\"Duration\", \"Price\") "
                             "VALUES(%s, %s, %s, %s, %s) RETURNING \"Id\"",
                             (int(tour.HotelId), datetime(tour.DepartureDate), tour.DepartureCity, int(tour.Duration),
                              int(tour.Price)))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("tour", "add")
            return
        return id

    def AddOrder(self, order):
        self.cur.execute("SELECT * FROM \"Client\" WHERE \"Id\" = {:d}".format((order.ClientId)))
        check = self.cur.fetchall()
        if check == None:
            print("Order with given id was not added. Please, firstly add the corresponding client.")
            return False
        self.cur.execute("SELECT * FROM \"Tour\" WHERE \"Id\" = {:d}".format((order.TourId)))
        check = self.cur.fetchall()
        if check == None:
            print("Order with given id was not added. Please, firstly add the corresponding tour.")
            return False
        try:
            self.cur.execute("INSERT INTO public.\"Order\" (\"ClientId\", \"TourId\", \"PurchaseDate\", \"IsPaid\") "
                             "VALUES(%s, %s, %s, %s, %s) RETURNING \"Id\"",
                             (order.ClientId, order.TourId, order.PurchaseDate, order.IsPaid))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("order", "add")
            return
        return id

    def AddClient(self, client):
        try:
            self.cur.execute("INSERT INTO public.\"Client\" (\"FullName\", \"Passport\", \"Email\", \"Phone\") "
                             "VALUES(%s, %s, %s, %s) RETURNING \"Id\"",
                             (client.FullName, client.Passport, client.Email, client.Phone))
        except(Exception, psycopg2.Error):
            self.Exceptions("client", "add")
            return
        id = self.cur.fetchone()[0]
        self.con.commit()
        return id

    def UpdateCountry(self, country):
        try:
            self.cur.execute("UPDATE public.\"Country\" SET \"Name\" = '{0}', \"IsoCode\" = '{1}', "
                             "\"CurrencyIsoCode\" = '{2}', \"VisaRequired\" = '{3}' "
                             "WHERE \"Id\" = '{4}'".format(country.Name, country.IsoCode, country.CurrencyIsoCode,
                                                           country.VisaRequired, country.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("country", "update")
            return False
        return True

    def UpdateResort(self, resort):
        self.cur.execute("SELECT * FROM \"Country\" WHERE \"Id\" = {:d}".format((resort.CountryId)))
        check = self.cur.fetchall()
        if check == None:
            print("Resort with given id was not updated. Please, firstly add the corresponding country.")
            return False
        try:
            self.cur.execute("UPDATE public.\"Resort\" SET \"CountryId\" = '{0}', \"Name\" = '{1}' "
                             "WHERE \"Id\" = '{2}'".format(resort.CountryId, resort.Name, resort.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("resort", "update")
            return False
        return True

    def UpdateHotel(self, hotel):
        self.cur.execute("SELECT * FROM \"Resort\" WHERE \"Id\" = {:d}".format((hotel.ResortId)))
        check = self.cur.fetchall()
        if check == None:
            print("Hotel with given id was not updated. Please, firstly add the corresponding resort.")
            return False
        try:
            self.cur.execute("UPDATE public.\"Hotel\" SET \"ResortId\" = '{0}', \"Name\" = '{1}', \"Stars\" = '{2}' "
                             "WHERE \"Id\" = '{3}'".format(hotel.ResortId, hotel.Name, hotel.Stars, hotel.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("hotel", "update")
            return False
        return True

    def UpdateTour(self, tour):
        self.cur.execute("SELECT * FROM \"Hotel\" WHERE \"Id\" = {:d}".format((tour.HotelId)))
        check = self.cur.fetchall()
        if check == None:
            print("Tour with given id was not updated. Please, firstly add the corresponding hotel.")
            return False
        try:
            self.cur.execute(
                "UPDATE public.\"Tour\" SET \"HotelId\" = '{0}', \"DepartureDate\" = '{1}', \"DepartureCity\" = '{2}', "
                "\"Duration\" = '{3}', \"Price\" = '{4}', "
                "WHERE \"Id\" = '{5}'".format(tour.HotelId, tour.DepartureDate, tour.DepartureCity,
                                              tour.Duration, tour.Price, tour.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("tour", "update")
            return False
        return True

    def UpdateOrder(self, order):
        self.cur.execute("SELECT * FROM \"Client\" WHERE \"Id\" = {:d}".format((order.ClientId)))
        check = self.cur.fetchall()
        if check == None:
            print("Order with given id was not updated. Please, firstly add the corresponding client.")
            return False
        self.cur.execute("SELECT * FROM \"Tour\" WHERE \"Id\" = {:d}".format((order.TourId)))
        check = self.cur.fetchall()
        if check == None:
            print("Order with given id was not updated. Please, firstly add the corresponding tour.")
            return False
        try:
            self.cur.execute(
                "UPDATE public.\"Order\" SET \"ClientId\" = '{0}', \"TourId\" = '{1}', \"PurchaseDate\" = '{2}', "
                "\"IsPaid\" = '{3}' "
                "WHERE \"Id\" = '{4}'".format(order.ClientId, order.TourId, order.PurchaseDate,
                                              order.IsPaid, order.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("order", "update")
            return False
        return True

    def UpdateClient(self, client):
        try:
            self.cur.execute(
                "UPDATE public.\"Client\" SET \"FullName\" = '{0}', \"Passport\" = '{1}', \"Email\" = '{2}', \"Phone\" = '{3}' "
                "WHERE  \"Id\" = '{4}'".format(client.FullName, client.Passport, client.Email, client.Phone, client.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("client", "update")
            return False
        return True

    def DeleteEntity(self, entity):
        if entity.__class__.__name__ == "Country":
            self.cur.execute("SELECT * FROM \"Resort\" WHERE \"CountryId\" = {:d}".format((entity.Id)))
        if entity.__class__.__name__ == "Resort":
            self.cur.execute("SELECT * FROM \"Hotel\" WHERE \"ResortId\" = {:d}".format((entity.Id)))
        if entity.__class__.__name__ == "Hotel":
            self.cur.execute("SELECT * FROM \"Tour\" WHERE \"HotelId\" = {:d}".format((entity.Id)))
        if entity.__class__.__name__ == "Tour":
            self.cur.execute("SELECT * FROM \"Order\" WHERE \"TourId\" = {:d}".format((entity.Id)))
        if entity.__class__.__name__ == "Client":
            self.cur.execute("SELECT * FROM \"Order\" WHERE \"ClientId\" = {:d}".format((entity.Id)))
        check = self.cur.fetchone()
        if check != None:
            print(f"Entity with given id was not deleted. Please, firstly delete dependent entity: {check}.")
            return False
        try:
            print(entity.__class__.__name__, " ", entity.Id)
            self.cur.execute(
                "DELETE FROM public.\"{0}\" WHERE \"Id\" = {1}".format(entity.__class__.__name__, entity.Id))
            self.con.commit()
        except(Exception, psycopg2.Error):
            self.Exceptions("entity", "delet")
            return False
        return True

    def GenerateClient(self, amount):
        try:
            self.cur.execute("CREATE OR REPLACE FUNCTION add_random_client() RETURNS integer AS "
                             "$$ DECLARE fullname TEXT; DECLARE passport TEXT; DECLARE email TEXT; DECLARE phone TEXT; "
                             "BEGIN fullname = (SELECT upper(chr(trunc(65+random()*25)::int)) || "
                             "substr('abcdefghijklmnopqresuvwxyz',randomNumber(1,26)::INTEGER,5) || chr(32) ||  "
                             "upper(chr(trunc(65+random()*25)::int)) || "
                             "substr('abcdefghijklmnopqresuvwxyz',randomNumber(1,26)::INTEGER,6)); "
                             "passport = (SELECT upper(substr(md5(random()::text), 0, 9))); "
                             "email = (SELECT substr(md5(random()::text), 0, 19)); "
                             "phone = (SELECT TO_CHAR(ROUND(randomNumber(1111111111,9999999999)),'+9999999999')); "
                             "INSERT INTO public.\"Client\"(\"FullName\", \"Passport\", \"Email\", \"Phone\") "
                             "VALUES(fullname, passport, email, phone); RETURN 1; END; $$ LANGUAGE plpgsql; "
                             "SELECT * FROM  generate_series(1, '{0}') WHERE add_random_client() = 1".format(
                int(amount)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Problem generating clients.")
            return False
        print("Generated clients.")
        return True

    def GenerateTour(self, amount):
        try:
            self.cur.execute("CREATE OR REPLACE FUNCTION add_random_tour() RETURNS integer  AS "
                             "$$ DECLARE hotel_id INTEGER; DECLARE  deprtrdate DATE; "
                             "DECLARE deprtrcity TEXT; DECLARE duration INTEGER; DECLARE price INTEGER; BEGIN "
                             "hotel_id = (SELECT \"Id\" FROM public.\"Hotel\" ORDER BY RANDOM() LIMIT 1); "
                             "deprtrdate = (SELECT TO_DATE((TO_CHAR(CURRENT_TIMESTAMP,'J')::INTEGER + "
                             "ROUND(randomNumber(1,365)))::TEXT,'J')); "
                             "deprtrcity = (SELECT upper(chr(trunc(65+random()*25)::int)) || "
                             "substr('abcdefghijklmnopqresuvwxyz',randomNumber(1,26)::INTEGER,5)); "
                             "duration = (SELECT randomNumber(1, 14)::INTEGER); "
                             "price = (SELECT randomNumber(1000, 14000)::INTEGER); "
                             "INSERT INTO public.\"Tour\"(\"HotelId\", \"DepartureDate\", \"DepartureCity\", \"Duration\", \"Price\") "
                             "VALUES(hotel_id, deprtrdate, deprtrcity, duration, price); RETURN 1; END; $$ LANGUAGE plpgsql; "
                             "SELECT * FROM  generate_series(1, '{0}') WHERE add_random_tour() = 1".format(int(amount)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Problem generating tours.")
            return False
        print("Generated tours.")
        return True

    def GenerateOrder(self, amount):
        try:
            self.cur.execute("CREATE OR REPLACE FUNCTION add_random_order() RETURNS integer  "
                             "AS $$ DECLARE client_id INTEGER; "
                             "DECLARE  tour_id INTEGER; DECLARE purchdate DATE; DECLARE ispaid BOOLEAN; BEGIN "
                             "client_id = (SELECT \"Id\" FROM public.\"Client\" ORDER BY RANDOM() LIMIT 1); "
                             "tour_id = (SELECT \"Id\" FROM public.\"Tour\" ORDER BY RANDOM() LIMIT 1); "
                             "purchdate = (SELECT (CURRENT_DATE-(ROUND(randomNumber(1,29))||' DAYS')::INTERVAL)::DATE); "
                             "ispaid = (SELECT randomBoolean()); "
                             "INSERT INTO public.\"Order\"(\"ClientId\", \"TourId\", \"PurchaseDate\", \"IsPaid\")"
                             "VALUES(client_id, tour_id, purchdate, ispaid); RETURN 1; END; $$ LANGUAGE plpgsql; "
                             "SELECT * FROM  generate_series(1, '{0}') WHERE add_random_order() = 1".format(
                int(amount)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Problem generating orders.")
            return False
        print("Generated orders.")
        return True

    def FindTourHotel(self, priceMin, priceMax, duration, stars):
        try:
            before_query = datetime.datetime.now()
            self.cur.execute(
                "SELECT * from (SELECT \"Id\", \"Name\" from public.\"Hotel\" WHERE \"Stars\" = '{0}' ) as hotel "
                "INNER JOIN (SELECT \"Id\",\"HotelId\" from public.\"Tour\" WHERE \"Price\" > '{1}' and \"Price\" < '{2}' and "
                "\"Duration\" = '{3}') as tour ON hotel.\"Id\" = tour.\"HotelId\"".format(int(stars), int(priceMin),
                                                                                          int(priceMax),
                                                                                          int(duration)))
            after_query = datetime.datetime.now()
            queryTime = after_query - before_query
            print("Time for query: '{0}'\n".format(queryTime))
            info = self.cur.fetchall()
            print("Hotel Id | Hotel Name | Tour Id")
            for i in range(0, len(info)):
                print(f"{int(info[i][0])} | {info[i][1]} | {info[i][2]}")
        except(Exception, psycopg2.Error):
            print("Troubles in finding. Please check your input.")
            return False
        return True

    def FindClientPaid(self, priceMin, priceMax, isPaid):
        try:
            before_query = datetime.datetime.now()
            self.cur.execute("SELECT * from (SELECT \"Id\" from public.\"Tour\" "
                             "WHERE \"Price\" > '{0}' and \"Price\" < '{1}') as tour "
                             "INNER JOIN (SELECT \"Id\",\"ClientId\",\"TourId\" from public.\"Order\" "
                             "WHERE \"IsPaid\" = '{2}'::BOOLEAN) as order_ ON tour.\"Id\" = order_.\"TourId\" "
                             "INNER JOIN (SELECT \"Id\",\"FullName\" from public.\"Client\") as client "
                             "ON client.\"Id\" = order_.\"ClientId\"".format(priceMin, priceMax, int(isPaid)))
            after_query = datetime.datetime.now()
            queryTime = after_query - before_query
            print("Time for query: '{0}'\n".format(queryTime))
            info = self.cur.fetchall()
            print("Client Id | Client FullName | Tour Id")
            for i in range(0, len(info)):
                print(f"{int(info[i][2])} | {info[i][5]} | {info[i][3]}")
        except(Exception, psycopg2.Error):
            print("Troubles in finding. Please check your input.")
            return False
        return True

    def FindCountryHotels(self, cntrIsoCode, stars):
        try:
            before_query = datetime.datetime.now()
            self.cur.execute("SELECT * FROM (SELECT \"Id\",\"Name\" from public.\"Country\" "
                             "WHERE \"IsoCode\" = '{0}'::TEXT ) as country INNER JOIN (SELECT \"Id\",\"CountryId\", "
                             "\"Name\" from public.\"Resort\") as resort ON country.\"Id\" = resort.\"CountryId\" "
                             "INNER JOIN (SELECT \"Id\",\"ResortId\",\"Name\" from public.\"Hotel\" "
                             "WHERE \"Stars\" = '{1}') as hotel ON resort.\"Id\" = hotel.\"ResortId\"".format(
                cntrIsoCode, stars))
            after_query = datetime.datetime.now()
            queryTime = after_query - before_query
            print("Time for query: '{0}'\n".format(queryTime))
            info = self.cur.fetchall()
            print("Country Id | Country Name | Resort Id | Resort Name | Hotel Id | Hotel Name")
            for i in range(0, len(info)):
                print(f"{int(info[i][0])} | {info[i][1]} | {info[i][2]} | {info[i][4]} | {info[i][5]} | {info[i][7]}")
        except(Exception, psycopg2.Error):
            print("Troubles in finding. Please check your input.")
            return False
        return True
