import datetime


class Client:
    def __init__(self):
        self.Id = -1
        self.Fullname = ""
        self.Passport = ""
        self.Email = ""
        self.Phone = ""


class Country:
    def __init__(self):
        self.Id = -1
        self.Name = ""
        self.IsoCode = ""
        self.CurrencyIsoCode = ""
        self.VisaRequired = False


class Hotel:
    def __init__(self):
        self.Id = -1
        self.ResortId = -1
        self.Name = ""
        self.Stars = -1


class Order:
    def __init__(self):
        self.Id = -1
        self.ClientId = -1
        self.TourId = -1
        self.PurchaseDate = datetime.datetime.now()
        self.IsPaid = False


class Resort:
    def __init__(self):
        self.Id = -1
        self.CountryId = -1
        self.Name = ""


class Tour:
    def __init__(self):
        self.Id = -1
        self.HotelId = -1
        self.DepartureDate = datetime.datetime.now()
        self.DepartureCity = ""
        self.Duration = -1
        self.Price = -1
