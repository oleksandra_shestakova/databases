import datetime
import psycopg2
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Column, Integer, String, Boolean, Date, Table, ForeignKey

Base = declarative_base()


class Client(Base):
    __tablename__ = 'Client'
    Id = Column(Integer, primary_key=True)
    FullName = Column(String)
    Passport = Column(String)
    Email = Column(String)
    Phone = Column(String)

    def __init__(self, fullname, passport, email, phone):
        self.Id = -1
        self.FullName = fullname
        self.Passport = passport
        self.Email = email
        self.Phone = phone


class Country(Base):
    __tablename__ = 'Country'
    Id = Column(Integer, primary_key=True)
    Name = Column(String)
    IsoCode = Column(String)
    CurrencyIsoCode = Column(String)
    VisaRequired = Column(Boolean)

    def __init__(self, name, isocode, currencyisocode, visarequired):
        self.Name = name
        self.IsoCode = isocode
        self.CurrencyIsoCode = currencyisocode
        self.VisaRequired = visarequired


class Resort(Base):
    __tablename__ = 'Resort'
    Id = Column(Integer, primary_key=True)
    CountryId = Column(Integer, ForeignKey('Country.Id'))
    country = relationship("Country", backref="Resort")
    Name = Column(String)

    def __init__(self, countryid, name):
        self.CountryId = countryid
        self.Name = name


class Hotel(Base):
    __tablename__ = 'Hotel'
    Id = Column(Integer, primary_key=True)
    ResortId = Column(Integer, ForeignKey('Resort.Id'))
    resort = relationship("Resort", backref="Hotel")
    Name = Column(String)
    Stars = Column(Integer)

    def __init__(self, resortid, name, stars):
        self.ResortId = resortid
        self.Name = name
        self.Stars = stars


class Tour(Base):
    __tablename__ = 'Tour'
    Id = Column(Integer, primary_key=True)
    HotelId = Column(Integer, ForeignKey('Hotel.Id'))
    hotel = relationship("Hotel", backref="Tour")
    DepartureDate = Column(Date)
    DepartureCity = Column(String)
    Duration = Column(Integer)
    Price = Column(Integer)

    def __init__(self, hotelid, departuredate, departurecity, duration, price):
        self.HotelId = hotelid
        self.DepartureDate = departuredate
        self.DepartureCity = departurecity
        self.Duration = duration
        self.Price = price


class Order(Base):
    __tablename__ = 'Order'
    Id = Column(Integer, primary_key=True)
    ClientId = Column(Integer, ForeignKey('Client.Id'))
    client = relationship("Client", backref="Order")
    TourId = Column(Integer, ForeignKey('Tour.Id'))
    tour = relationship("Tour", backref="Order")
    PurchaseDate = Column(Date)
    IsPaid = Column(Boolean)

    def __init__(self, clientid, tourid, purchasedate, ispaid):
        self.ClientId = clientid
        self.TourId = tourid
        self.PurchaseDate = purchasedate
        self.IsPaid = ispaid
