from python_console_menu import *
from colr import *
from orm import *
from entities import *

db = DataBase()
db.connect()


class CRUDMenu(AbstractMenu):
    def __init__(self, entityName_):
        self.entityName = entityName_
        super().__init__("Choose an option:")

    def getBool(self, input_):
        if (input_.lower() == "true" or int(input_) == 1):
            return True
        else:
            return False

    def AddEntityInfo(self, entityName_):
        entity = -1
        if entityName_ == "Country":
            entity = Country(input("Country name: "), input("Country ISO code: "), input("Currency ISO code:"),
                             self.getBool(input("Is visa required: ")))
        elif entityName_ == "Resort":
            entity = Resort(input("Country id: "), input("Resort name: "))
        elif entityName_ == "Hotel":
            entity = Hotel(int(input("Resort id: ")), input("Hotel name:"), int(input("Amount of stars: ")))
        elif entityName_ == "Tour":
            entity = Tour(int(input("Hotel id: ")),
                          datetime.strptime(input("Departure date (%m-%d-%Y): "), '%m-%d-%Y').date(),
                          input("Departure city: "),
                          int(input("Duration of stay:")), int(input("Tour price: ")))
        elif entityName_ == "Order":
            entity = Order(int(input("Client id: ")), int(input("Tour id: ")),
                           datetime.strptime(input("Purchase date (%m-%d-%Y): "), '%m-%d-%Y').date(),
                           self.getBool(input("Is order paid: ")))
        elif entityName_ == "Client":
            entity = Client(input("Full name:"), input("Passport: "), input("Email: "), input("Phone: "))
        if entity == -1:
            print("Wrong entity name.")
            return False
        return db.AddEntity(entity)

    def UDEntityInfo(self, action, entityName_, entityId):
        entity = db.GetEntityById(entityName_, entityId)
        if action == "update":
            if entity == False:
                return False
            if entityName_ == "Country":
                entity.Name = input("Country name: ")
                entity.IsoCode = input("Country ISO code: ")
                entity.CurrencyIsoCode = input("Currency ISO code:")
                entity.VisaRequired = self.getBool(input("Is visa required: "))
                return db.UpdateCountry(entity)
            if entityName_ == "Resort":
                entity.CountryId = input("Country id: ")
                entity.Name = input("Resort name: ")
                return db.UpdateResort(entity)
            if entityName_ == "Hotel":
                entity.ResortId = int(input("Resort id: "))
                entity.Name = input("Hotel name:")
                entity.Stars = int(input("Amount of stars: "))
                return db.UpdateHotel(entity)
            if entityName_ == "Tour":
                entity.HotelId = int(input("Hotel id: "))
                entity.DepartureDate = datetime.strptime(input("Departure date (%m-%d-%Y): "), '%m-%d-%Y').date()
                entity.DepartureCity = input("Departure city: ")
                entity.Duration = int(input("Duration of stay:"))
                entity.Price = int(input("Tour price: "))
                return db.UpdateTour(entity)
            if entityName_ == "Order":
                entity.ClientId = int(input("Client id: "))
                entity.TourId = int(input("Tour id: "))
                entity.PurchaseDate = datetime.strptime(input("Purchase date (%m-%d-%Y): "), '%m-%d-%Y').date()
                entity.IsPaid = self.getBool(input("Is order paid: "))
                return db.UpdateOrder(entity)
            if entityName_ == "Client":
                entity.FullName = input("Full name:")
                entity.Passport = input("Passport: ")
                entity.Email = input("Email: ")
                entity.Phone = input("Phone: ")
                return db.UpdateClient(entity)
            print("Problem updating entity: given entity name was not found.")
            return False
        elif action == "delete":
            db.DeleteEntity(entity)

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Get entity", lambda: db.GetEntityById(self.entityName, int(input("Id:")))))
        self.add_menu_item(MenuItem(102, "Add entity", lambda: self.AddEntityInfo(self.entityName)))
        self.add_menu_item(
            MenuItem(103, "Update entity", lambda: self.UDEntityInfo("update", self.entityName, input("Id:"))))
        self.add_menu_item(
            MenuItem(104, "Delete entity", lambda: self.UDEntityInfo("delete", self.entityName, input("Id:"))))


class EntityMenu(AbstractMenu):
    def __init__(self):
        super().__init__("Choose the entity to work with:")

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Country", menu=CRUDMenu("Country")))
        self.add_menu_item(MenuItem(102, "Resort", menu=CRUDMenu("Resort")))
        self.add_menu_item(MenuItem(103, "Hotel", menu=CRUDMenu("Hotel")))
        self.add_menu_item(MenuItem(104, "Tour", menu=CRUDMenu("Tour")))
        self.add_menu_item(MenuItem(105, "Order", menu=CRUDMenu("Order")))
        self.add_menu_item(MenuItem(106, "Client", menu=CRUDMenu("Client")))


class MainMenu(AbstractMenu):
    def __init__(self):
        super().__init__(color("---MAIN MENU---", fore='00f8fc'))

    def getBool(self, input_):
        if (input_.lower() == "true" or int(input_) == 1):
            return True
        else:
            return False

    def initialise(self):
        self.add_menu_item(MenuItem(100, color("Exit program", fore='917a81')).set_as_exit_option())
        self.add_menu_item(MenuItem(101, color("CRUD entity menu", fore='ebc634'), menu=EntityMenu()))
