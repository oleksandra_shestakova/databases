import psycopg2
from datetime import time, timedelta, datetime
from entities import *
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class DataBase:

    def connect(self):
        self.engine = create_engine('postgresql://postgres:DipperS13@localhost:5432/TourAgency')
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()

    def close(self):
        self.session.close()
        print("Connection to the database is closed.")

    def GetEntityById(self, entityName, id):
        entity = -1
        if entityName == "Country":
            entity = self.session.query(Country).get(id)
        if entityName == "Resort":
            entity = self.session.query(Resort).get(id)
        if entityName == "Hotel":
            entity = self.session.query(Hotel).get(id)
        if entityName == "Tour":
            entity = self.session.query(Tour).get(id)
        if entityName == "Order":
            entity = self.session.query(Order).get(id)
        if entityName == "Client":
            entity = self.session.query(Client).get(id)
        for attr, value in entity.__dict__.items():
            print(attr,": ", value)
        return entity

    def AddEntity(self, entity):
        self.session.add(entity)
        self.session.commit()
        print("Entity added successfully.")
        return entity.Id

    def UpdateCountry(self, country):
        some_country = self.GetEntityById("Country", country.Id)
        some_country.Name = country.Name
        some_country.IsoCode = country.IsoCode
        some_country.CurrencyIsoCode = country.CurrencyIsoCode
        some_country.VisaRequired = country.VisaRequired
        self.session.commit()
        return True

    def UpdateResort(self, resort):
        some_resort = self.GetEntityById("Resort", resort.Id)
        some_resort.CountryId = resort.CountryId
        some_resort.Name = resort.Name
        self.session.commit()
        return True

    def UpdateHotel(self, hotel):
        some_hotel = self.GetEntityById("Hotel", hotel.Id)
        some_hotel.ResortId = hotel.ResortId
        some_hotel.Name = hotel.Name
        some_hotel.Stars = hotel.Stars
        self.session.commit()
        return True

    def UpdateTour(self, tour):
        some_tour = self.GetEntityById("Tour", tour.Id)
        some_tour.HotelId = tour.HotelId
        some_tour.DepartureDate = tour.DepartureDate
        some_tour.DepartureCity = tour.DepartureCity
        some_tour.Duration = tour.Duration
        some_tour.Price = tour.Price
        self.session.commit()
        return True

    def UpdateOrder(self, order):
        some_order = self.GetEntityById("Order", order.Id)
        some_order.ClientId = order.ClientId
        some_order.TourId = order.TourId
        some_order.PurchaseDate = order.PurchaseDate
        some_order.IsPaid = order.IsPaid
        self.session.commit()
        return True

    def UpdateClient(self, client):
        some_client = self.GetEntityById("Client", client.Id)
        some_client.FullName = client.FullName
        some_client.Passport = client.Passport
        some_client.Email = client.Email
        some_client.Phone = client.Phone
        self.session.commit()
        return True

    def DeleteEntity(self, entity):
        self.session.delete(entity)
        self.session.commit()
        return True
